This is a porting of the [react tutorial](https://reactjs.org/tutorial/tutorial.html) into Vue 3.

The scope of this project is only educational, to exercise my Vue skills.

The Vue 2 version is available [here](https://github.com/mirkoperillo/vue-tic-tac-toe)

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

## License

This project is release under CC0 license (Creative Commons Zero)

