/*
  tic-tac-toe - a Vue 3 exercise
  Written in 2023 by Mirko Perillo
  To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide.
  This software is distributed without any warranty.
  You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/
import { defineStore } from 'pinia'
import Snapshot from '@/model/Snapshot'
import { Grid } from '@/model/Grid'
import { GameLocation } from '@/model/GameLocation'

export interface State {
  xIsNext: boolean,
  historyList: Snapshot[],
  grid: Grid,
  sortHistoryAscending: boolean,
  isGameOver: boolean,
  playerWinner: string,
  lineWinner: number[],
  move: number,
  historyMove: boolean
}
export const useGameStore = defineStore('game', {
  state: () => ({
    xIsNext: true,
    historyList: [new Snapshot()],
    grid: new Grid(),
    sortHistoryAscending: true,
    isGameOver: false,
    playerWinner: '',
    lineWinner: [],
    move: 0,
    historyMove: false
  } as State),
  actions: {
    playTurn(player: string, location: GameLocation) {
      this.xIsNext = !this.xIsNext
      this.move += 1
      this.grid.pos[location.row * 3 + location.col] = player
      this.historyList = [...this.historyList.slice(0, this.move), new Snapshot(this.move, location, this.grid)]
      this.historyMove = false
    },
    sortHistory() {
      this.sortHistoryAscending = !this.sortHistoryAscending
    },
    gameOver() {
      this.isGameOver = true
    },
    setWinner(winner: any) {
      this.playerWinner = winner.player
      this.lineWinner = winner.line
    },
    jumpToHistoryMove(move: number) {
      this.move = move
      this.grid.pos = this.historyList[move].grid.pos.slice()
      this.historyMove = true
      this.xIsNext = move % 2 === 0
    }
  },
  getters: {
    currentPlayer: (state) => state.xIsNext ? 'X' : 'O',
    tie: (state) => state.historyList.length === 10,
    currentMove: (state) => state.move,
  }
})
