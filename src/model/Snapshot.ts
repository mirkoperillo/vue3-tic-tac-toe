/*
  tic-tac-toe - a Vue 3 exercise
  Written in 2023 by Mirko Perillo
  To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide.
  This software is distributed without any warranty.
  You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/
import { GameLocation } from "./GameLocation";
import { Grid } from "./Grid";

export default class Snapshot {
  move: number;
  location: GameLocation;
  grid: Grid;

  constructor(move: number = 0, location: GameLocation = new GameLocation, grid: Grid = new Grid) {
    this.move = move;
    this.location = location;
    this.grid = new Grid();
    this.grid.pos = grid.pos.slice();
  }
}

